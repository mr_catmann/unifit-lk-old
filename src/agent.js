import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'http://api.unifit24.ru';

const encode = encodeURIComponent;
const responseBody = res => res.body;

let token = null;
const tokenPlugin = req => {
  if (token) {
    req.set('Authorization', `Bearer ${token}`);
  }
}

const requests = {
  del: url =>
    superagent.del(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  get: url =>
    superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  put: (url, body) =>
    superagent.put(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
  post: (url, body) =>
    superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody)
};

const Auth = {
  current: () =>
    requests.get('/users/myinfo'),
  login: (email, password) =>
    requests.post('/users/login', { user: { email, password } }),
  register: (username, email, password) =>
    requests.post('/users', { user: { username, email, password } }),
  save: user =>
    requests.put('/user', { user })
};

const Plots = {
	getDynamics: () =>
		requests.get('/measurements/getdynamics'),
}

const Schedule = {
	today: () =>
		requests.get('/users/getschedule'),
	byDay: (day) =>
		requests.post('/users/getschedule',{data:{day:day}})	
}
const Measurement = {
	data: () =>
		requests.get('/measurements/getdata'),
	send: (data) =>
		requests.post('/measurements/send',{data})	
}

const Trainings = {
	get: (id) =>
		requests.get('/trainings/get/'+id),
}

const Menu = {
	get: (id) =>
		requests.get('/menu/get/'+id),
}

const Settings = {
	save: (data) =>
		requests.post('/users/edit',{data})	
}

const Recipes = {
	getCategories: () =>
		requests.get('/recipes/getcategories'),
	getByCategory: (id) =>	
		requests.get('/recipes/getbycategory/'+id),	
	getById: (id) =>	
		requests.get('/recipes/get/'+id)		
}

const Register = {
	checkRegistrationCode: (code) =>
		requests.post('/users/checkregistrationcode',{data:{code:code}}),
	changePassword: (data) =>
		requests.post('/users/changepassword',{data}),
	saveFormData: (data) =>
		requests.post('/users/saveformdata',{data}),
}


export default {
  Auth,
  Measurement,
  Menu,
  Trainings,
  Plots,
  Recipes,
  Schedule,
  Register,
  Settings,
  setToken: _token => { token = _token; }
};

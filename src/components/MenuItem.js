import React from 'react';
import { connect } from 'react-redux';
import Preloader from './Preloader';
import Error from './Error';
import * as menuActions from '../actions/menu';
import { bindActionCreators } from 'redux'
const mapStateToProps = (state => {
	return{ 
		...state.menu
	}
});

const mapDispatchToProps = dispatch => ({
	 actions: bindActionCreators(menuActions, dispatch)
});


class MenuItem extends React.Component {
  constructor() {
		super();
		this.showHideIngredients = this.showHideIngredients.bind(this);
		this.state = {
			//currentMealItemIndex:0,
			itemsWithShownIngredients:{},
		};
  }
  
  showHideIngredients(i) {
		let itemsWithShownIngredients = this.state.itemsWithShownIngredients;
		itemsWithShownIngredients[i] = !itemsWithShownIngredients[i];
		this.setState({
			itemsWithShownIngredients
		});
  }
  
  componentWillReceiveProps(props) {
	  if (props.index !== this.props.index){
		this.setState({
			itemsWithShownIngredients:[]
		});
	  }
	  if (!this.menu(props.id) && (!this.props.menu || !this.props.menu.loading)) {
		 this.props.actions.getMenu(props.id);
	  }
  }
  
  menu(id) {
		if (!id) {
			id = this.props.id;
		}
		if (!this.props.menu || !this.props.menu.items || !this.props.menu.items[id]) {
			return null;
		}
		return this.props.menu.items[id];
  }
  
  componentWillMount() {
	  if (!this.menu()) {
		   this.props.actions.getMenu(this.props.id);
	  }
  }
  
  render() {
	const menu = this.menu();
	const index = this.props.index;
	if (!menu) {
		return <div className="menu-item"><Preloader /></div>;
	}
	if (!menu.menu || !menu.menu[index]) {
		const data = {status:0,text:'Ошибка: не найдено меню'};
		return <Error data={data} />;
	}
	const meal = menu.menu[index];
	
	return (
	  <div className="menu-items">
		<div className="block-title">{meal.name}</div>
		<div className="menu-items__list">
		  {meal.items.map((dish, index) => {     
		  const dishCaloriesText = dish ? (dish.calories+" калорий, "+dish.count+" грамм, Б - "+dish.proteins+", Ж - "+dish.fats+", У - "+dish.carbohydrates) : "";
		  const dishInstruction = dish && dish.recipe ? {'__html':dish.recipe.instruction} : null;
		  return (
			  <div className="block block-item" key={index}>
						{dish.recipe && 
						<div className="block-item__left">					
							{ dish.recipe.picture && <img className="block-item__picture" src={dish.recipe.picture} />}
							<div className="block-item__left__buttons"><a onClick={(e) => this.showHideIngredients(index, e)} className="btn btn-contour btn-grey btn-medium">{!this.state.itemsWithShownIngredients[index] ? 'Ингредиенты ('+dish.recipe.ingredients.length+')' : 'Рецепт'}</a></div> 
						</div>
						}
						<div className="block-item__info">
							<div className="block-item__name">{dish.name}</div>
							<div className="block-item__energy-value">{dishCaloriesText}</div>
							{
								(this.state.itemsWithShownIngredients[index]) 
								? 
								<div className="block-item__ingredients-list">
								 {dish.recipe.ingredients.map((ingredient, i) => {     
								   return (
										<div key={ingredient.name} className="block-item__ingredient">
											<span className="block-item__ingredient__name">{ingredient.name}</span>
											<span className="block-item__ingredient__count">{ingredient.count}</span>
										</div>
								   ) 
								})}
								</div>
								: (dish.recipe && 
								<div className="block-item__text">
									<div className="block-item__text__header">Инструкция по приготовлению</div>
									<div className="block-item__text__contents" dangerouslySetInnerHTML={dishInstruction}></div>
								</div>
								)
							}
						</div>
			  </div>
		    )
	     }
	  )
	}
	</div>
	</div>
  )
 }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuItem);

import React from 'react';
import agent from '../agent';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { Form, Field } from 'react-final-form'
import Preloader from './Preloader';
import Response from './Response';
import SelectButtons from './SelectButtons';
import SweetAlert from 'sweetalert-react';
import "sweetalert/dist/sweetalert.css";
import { store } from '../store';
import { push } from 'react-router-redux';
import * as registerActions from '../actions/register';
const mapStateToProps = (state => {
	return{ 
		...state.register
	}
});

const mapDispatchToProps = dispatch => ({
	 actions: bindActionCreators(registerActions, dispatch)
});

class Register extends React.Component {
  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
	this.validate = this.validate.bind(this);
	this.stepsCount = 6;
	this.state = {
		currentStep:0,
		showAlert:true,
		isLoading:false,
		values:{
			
		}
	};
  }

  componentWillMount() {
	 let code = this.props.match.params.id;
	 this.props.actions.checkRegistrationCode(code);
  }
  
  componentWillReceiveProps(props) {
	  if (props.register.password && props.register.password.response && props.register.password.response.status && (!this.props.register.password.response || !this.props.register.password.response.status) && !props.register.save_form){
		 this.setState({
			currentStep:1, 
		 }); 
	  }
  }
  onSubmit(values) {
	  this.setState({
		  isLoading:true
	  });
	  if (this.state.currentStep === 0) {
			values.code = this.props.match.params.id;
			this.props.actions.savePassword(values);
	  } else {
		  if (this.state.currentStep < (this.stepsCount-1)) {
			  this.setState({
				  values:{...this.state.values,...values},
				  currentStep:this.state.currentStep+1
			  });
		  } else {
			  values.code = this.props.match.params.id;
			  this.props.actions.saveFormData(values);
		  }
	  }
  }
  
  validate(values){
	  const errors = {};
	  if (this.state.currentStep === 0) {
		  if (values.repeat_password !== values.password) {
			  errors.repeat_password = ["Пароли не совпадают"];
		  }
		  if (!values.password || values.password.length<5) {
			  errors.password = ["Пароль должен быть длиной хотя бы 5 символов"];
		  }
	  } else {
		  let fields = {
			  1:['fio','age','gender'],
			  2:['country','city'],
			  3:['reason','intentions'],
			  4:['nutrition_type','target_weight'],
			  5:['trainings_type']
		  }
		  fields[this.state.currentStep].forEach(field=>{
			  if (!values[field] || values[field] === "" || values[field] === 0) {
				  errors[field] = "Заполните это поле";
			  }
		  })
	  }
	  return errors;

  }
  
   _textInput(item,input,meta) {
	  return (
		  <div key={item.id}>
			<label htmlFor={item.id}>{item.name}</label>
			<div className="input-group">
				<input className={`form-control ${meta.touched && meta.error ? 'is-invalid' : ''}`}  {...input} type={item.type || "text"}  />
				<div className="input-group-append">
					{item.units && <span className="input-group-text">{item.units}</span>}
				</div>
				<div className="invalid-feedback">
						{meta.touched ? meta.error : ''}
				</div>													
			</div>
			<small className="form-text text-muted">{item.description}</small>
		 </div>
	  )
  }	
  
   _selectInput(item,input,meta) {
	  return (
		  <div key={item.id}>
			<label htmlFor={item.id}>{item.name}</label>
			<div className="input-group">
				<select className={`form-control ${meta.touched && meta.error ? 'is-invalid' : ''}`}  {...input} >
					<option value="">...</option>
					{
						item.variants.map(variant=> {
								return (
									<option key={variant} value={variant}>{variant}</option>
								)
						})
					}
				</select>
				<div className="invalid-feedback">
						{meta.touched ? meta.error : ''}
				</div>													
			</div>
			<small className="form-text text-muted">{item.description}</small>
		 </div>
	  )
  }
  
  render() {
	if (!this.props.register || !this.props.register.code || this.props.register.code.loading) {
		return <Preloader />;
	}
	if (this.props.register.code.error) {
		return <Response data={this.props.register.code.error} />;
	}
	let initialValues = {
		
	};
	
	const reasonVariants = ['Похудеть','Научиться правильно питаться','Накачать отдельные группы мышц','Восстановление после беременности'];
	const intentionsVariants = ['Серьезные, полная отдача','Попробовать'];
	return (
      <div >
			<SweetAlert
					show={!!(this.state.showAlert && this.props.register && this.props.register.save_form && this.props.register.save_form.response && this.props.register.save_form.response.status)}
					title="Сохранено!"
					onConfirm={() => {
						this.setState({
							showAlert:false
						});
						store.dispatch({ type: 'LOGIN', payload: this.props.register.save_form.response });
						store.dispatch(push('/'));
					}}
				  />
              {this.props.register.password && this.props.register.password.response && !this.props.register.password.response.status && <Response data={this.props.register.password.response} />}
			  {this.props.register.save_form && this.props.register.save_form.response  && <Response data={this.props.register.save_form.response} />}
			  
			  <Form validate={values => this.validate(values)}  initialValues={initialValues} onSubmit={this.onSubmit}  render={({ handleSubmit, form, submitting, pristine, values }) => (
					<form onSubmit={handleSubmit}> 
						{this.state.currentStep === 0 &&
						<div className="form-step">
							<div className="form-step__title">Придумайте пароль</div>
							<div className="form-step__description"></div>
							<Field
							name="password"
							render={({ input, meta }) => this._textInput({id:'password',type:'password',name:'Введите пароль'},input,meta)}
							/>
							<Field
							name="repeat_password"
							render={({ input, meta }) => this._textInput({id:'repeat_password',type:'password',name:'Повторите пароль'},input,meta)}
							/>
						</div> }
						{this.state.currentStep === 1 &&
						<div className="form-step">
							<div className="form-step__title">Теперь пришло время познакомиться</div>
							<div className="form-step__description"></div>
							<Field
							name="fio"
							render={({ input, meta }) => this._textInput({id:'fio',type:'text',name:'Твои имя и фамилия'},input,meta)}
							/>
							<Field
							name="age"
							render={({ input, meta }) => this._textInput({id:'age',type:'number',name:'Возраст'},input,meta)}
							/>
							<Field
								name="gender"
								title="Пол"
								variants={[{id:2,name:'Женский'},{id:1,name:'Мужской'}]}
								component={SelectButtons}
							/>
						</div> }
						{this.state.currentStep === 2 &&
						<div className="form-step">
							<div className="form-step__title">Откуда ты?</div>
							<div className="form-step__description">Возможно, именно в твоем городе мы проведем живую встречу</div>
							<Field
							name="country"
							render={({ input, meta }) => this._textInput({id:'country',type:'text',name:'Страна'},input,meta)}
							/>
							<Field
							name="city"
							render={({ input, meta }) => this._textInput({id:'city',type:'text',name:'Город'},input,meta)}
							/>
						</div> }
						{this.state.currentStep === 3 &&
						<div className="form-step">
							<div className="form-step__title">Какого результата ты хочешь достичь?</div>
							<div className="form-step__description"></div>
							<Field
							name="reason"
							render={({ input, meta }) => this._selectInput({id:'reason',name:'Цель',variants:reasonVariants},input,meta)}
							/>
							<Field
							name="intentions"
							render={({ input, meta }) => this._selectInput({id:'intentions',name:'Намерения',variants:intentionsVariants},input,meta)}
							/>
						</div> }
						{this.state.currentStep === 4 &&
						<div className="form-step">
							<div className="form-step__title">Выбери план питания</div>
							<div className="form-step__description">Правильное питание - фундамент похудения</div>
							<Field
								name="nutrition_type"
								title="Тип питания"
								variants={[{id:1,name:'Стандартное'},{id:2,name:'Вегетарианское'}]}
								component={SelectButtons}
							/>
							<Field
							name="target_weight"
							render={({ input, meta }) => this._textInput({id:'target_weight',type:'number',name:'Вес, которого вы хотите достигнуть',units:'кг'},input,meta)}
							/>
						</div> }
						{this.state.currentStep === 5 &&
						<div className="form-step">
							<div className="form-step__title">Выбери тип тренировкок</div>
							<div className="form-step__description"></div>
							<Field
								name="trainings_type"
								title="Тип тренировок"
								variants={[{id:1,name:'Стандартный тренинг'},{id:2,name:'Интенсивный тренинг'}]}
								component={SelectButtons}
							/>
						</div> }
						<div className="block">
							<div className="form-bottom">
								<div className="form-button-container">
									<button disabled={((this.props.register.password && this.props.register.password.loading))} type="submit" className="btn btn-lg btn-primary">
										{this.state.currentStep-1 === this.stepsCount ? <span>Сохранить</span> : <span>Далее</span>}
									</button>
								</div>
								<div className="form-progress-container">
									<div className="form-progress">
										<div className="progress">
										  <div className="progress-bar" role="progressbar" style={{"width":((this.state.currentStep+1)/this.stepsCount*100)+"%"}} aria-valuenow={(this.state.currentStep/this.stepsCount*100)} aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<div className="form-progress__title">Шаг {this.state.currentStep+1} из {this.stepsCount}</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				  )}
			  />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);

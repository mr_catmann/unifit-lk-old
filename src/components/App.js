import agent from '../agent';
import Header from './Header';
import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, Switch } from 'react-router-dom';

import Home from '../components/Home'; 
import Login from '../components/Login';
import Register from '../components/Register';
import Settings from '../components/Settings';
import Measurement from '../components/Measurement';
import Menu from '../components/Menu';
import RecipesCategories from '../components/Recipes/RecipesCategories';
import RecipesCategory from '../components/Recipes/RecipesCategory';
import Recipe from '../components/Recipes/Recipe';
import ErrorPage from '../components/ErrorPage';

import { store } from '../store';
import { push } from 'react-router-redux';


const mapStateToProps = state => {
  return {
    appLoaded: state.common.appLoaded,
    appName: state.common.appName,
    currentUser: state.common.currentUser,
    redirectTo: state.common.redirectTo
  }};

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, token) =>
    dispatch({ type: 'APP_LOAD', payload, token}),
  onRedirect: () =>
    dispatch({ type: 'REDIRECT' })
});

class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      // this.context.router.replace(nextProps.redirectTo);
      store.dispatch(push(nextProps.redirectTo));
      this.props.onRedirect();
    }
  }

  componentWillMount() {
    const token = window.localStorage.getItem('jwt');
    if (token) {
      agent.setToken(token);
    }

    this.props.onLoad(token ? agent.Auth.current() : null, token);
  }

  render() {
    if (this.props.appLoaded) {
      return (
        <div>
          <Header
            appName={this.props.appName}
            currentUser={this.props.currentUser} />
			<div className="container">
				<Switch>
					<Route path="/login" component={Login} />
					<Route path="/register/:id" component={Register} />
					{this.props.currentUser ? (
					 <React.Fragment>
						<Route exact path="/" component={Home}/>
						<Route exact path="/menu" component={Menu} />
						<Route exact path="/measurement" component={Measurement} />
						<Route exact path="/settings" component={Settings} />
					    <Route exact path="/nutrition/category/:id" component={RecipesCategory} />	
						<Route exact path="/nutrition/recipes/:id" component={Recipe} />	
						<Route exact path="/nutrition" component={RecipesCategories} />
					</React.Fragment>
					 ) : (
						<Redirect to="/login" />
					)}
				</Switch>
			</div>
        </div>
      );
    }
    return (
      <div>
        <Header
          appName={this.props.appName}
          currentUser={this.props.currentUser} />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

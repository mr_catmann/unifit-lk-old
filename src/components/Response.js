import React from 'react';

class Response extends React.Component {
  render() {
    const data = this.props.data;
	if (data && data.status !== undefined) {
      return (
        <div className={"alert alert-" + (data.status === 1 ? 'success' : 'danger')}>
			{data.text}
        </div>
      );
    } else {
      return null;
    }
  }
}

export default Response;

import React from 'react';

class Error extends React.Component {
  render() {
    const data = this.props.data;
	if (data && data.status !== undefined) {
      return (
        <div className="alert alert-danger">
			{data.text}
        </div>
      );
    } else {
      return null;
    }
  }
}

export default Error;

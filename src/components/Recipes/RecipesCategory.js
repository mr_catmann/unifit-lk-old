import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Preloader from '../Preloader';
import Response from '../Response'; 
import * as recipesActions from '../../actions/recipes';
import { bindActionCreators } from 'redux';
const mapStateToProps = (state => {
	return{ 
		...state.recipes
	}
});

const mapDispatchToProps = dispatch => ({
	 actions: bindActionCreators(recipesActions, dispatch)
});

class RecipesCategory extends React.Component {
	constructor() {
		super();
		this.state = {
			
		};
	} 
  
  recipesByCategory(id) {
	  if (!this.props.recipes || !this.props.recipes.listsByCategory ||  !this.props.recipes.listsByCategory.data || !this.props.recipes.listsByCategory.data[id] || this.props.recipes.listsByCategory.loading) {
		  return null;
	  }
	  return this.props.recipes.listsByCategory.data[id];
	 
  }
  
  componentWillMount() {
		const id = this.props.match.params.id;
		if (!this.recipesByCategory(id)) {
			 this.props.actions.getRecipesByCategory(id);
		}
  }
  
  render() {
	const id = this.props.match.params.id;
	if (!id) {
		const err = {status:0,text:'Ошибка: укажите название категории'};
		return <Response data={err} />
	} 
	let recipes = this.recipesByCategory(id);
	
	if (this.props.recipes && this.props.recipes.listsByCategory && this.props.recipes.listsByCategory.error) {
		return <Response data={this.props.recipes.listsByCategory.error} />
	}
	if (!recipes) {
		return <div className="recipes"><Preloader /></div>;
	}
	return <div className="recipes">
		<h3 className="mt-5 mb-5">{recipes.category_title}</h3>
		<div className="row">
		{
			recipes.data.map((recipe, index) => {  	
				const energyValues = "Калорийность - "+recipe.energy_value['Калорийность']+", белки -  "+recipe.energy_value['Белки']+", жиры - "+recipe.energy_value['Жиры']+", углеводы - "+recipe.energy_value['Углеводы'];    			
				return (
					<div key={recipe.id} className="col-xs-12 col-md-6">
							<Link to={"/nutrition/recipes/"+recipe.id} className="block recipes__category">
								<div className="recipes__category__picture" style={{background:(recipe.picture ? 'url('+recipe.picture+') no-repeat center center' : ''),backgroundSize:'cover'}}></div>
								<div className="block-title">{recipe.title}</div>
								<div className="recipes__category__energy-values">{energyValues}</div>
								<div className="recipes__category__time-cook">Время приготовления: {recipe.time_cook}</div>
							</Link>
				      </div>
				)
			})
		}
		</div>
	</div>
 }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipesCategory);

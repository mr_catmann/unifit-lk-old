import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Preloader from '../Preloader';
import Response from '../Response'; 
import * as recipesActions from '../../actions/recipes';
import { bindActionCreators } from 'redux';
const mapStateToProps = (state => {
	return{ 
		...state.recipes
	}
});

const mapDispatchToProps = dispatch => ({
	 actions: bindActionCreators(recipesActions, dispatch)
});

class Recipe extends React.Component {
	constructor() {
		super();
		this.state = {
			
		};
	} 
  
  recipeById(id) {
	  if (!this.props.recipes || !this.props.recipes.list ||  !this.props.recipes.list.data || !this.props.recipes.list.data[id] || this.props.recipes.list.loading) {
		  return null;
	  }
	  return this.props.recipes.list.data[id];
	 
  }
  
  componentWillMount() {
		const id = this.props.match.params.id;
		if (!this.recipeById(id)) {
			 this.props.actions.getRecipeById(id);
		}
  }
  componentWillReceiveProps(props) {
		const newID = props.match.params.id;
		if (newID !== this.props.match.params.id && !this.recipeById(newID)) {
			 this.props.actions.getRecipeById(newID);
		}
  }
  render() {
	const id = this.props.match.params.id;
	if (!id) {
		const err = {status:0,text:'Ошибка: укажите название категории'};
		return <Response data={err} />
	} 
	if (this.props.recipes && this.props.recipes.list && this.props.recipes.list.error) {
		return <Response data={this.props.recipes.list.error} />
	}
	let recipe = this.recipeById(id);
	if (!recipe) {
		return <div className="recipe"><Preloader /></div>;
	}
	const similar = recipe.similar;
	recipe = recipe.data;
	const energyValues = "К - "+recipe.energy_value['Калорийность']+", Б -  "+recipe.energy_value['Белки']+", Ж - "+recipe.energy_value['Жиры']+", У - "+recipe.energy_value['Углеводы'];    							
	return (
		<div className="recipe">
			<h2 className="mt-5 mb-3">{recipe.title}</h2>
			{recipe.picture &&
				<div className="recipe__picture" style={{background:('url('+recipe.picture+') no-repeat center center'),backgroundSize:'cover'}} />
			}
			<div className="recipe__info">
				<div className="recipe__time-cook">
					<i className="fa fa-clock-o"></i>
					<span className="recipe__time-cook__text">{recipe.time_cook}</span>
				</div>
				<div className="recipe__energy-values">
					<span className="recipe__energy-values__text">{energyValues}</span>
				</div>
			</div>
			<div className="recipe__texts">
				{recipe.description && recipe.description.length>0 && 
					<div className="recipe__texts__item">
						<h4>Описание</h4>
						<div className="recipe__texts__item__contents" dangerouslySetInnerHTML={{'__html':recipe.description}}></div> 
					</div>
				}
				{recipe.instruction && recipe.instruction.length>0 && 
					<div className="recipe__texts__item">
						<h4>Инструкция по приготовлению</h4>
						<div className="recipe__texts__item__contents" dangerouslySetInnerHTML={{'__html':recipe.instruction}}></div> 
					</div>
				}
			</div>
			
			{similar &&  
				<div className="recipe__similar">
					<h4>Еще рецепты из этой категории</h4>
					<div className="row"> 
						{
							similar.map((similarRecipe, index) => {  	
								return (
									<div key={similarRecipe.id} className="col-xs-12 col-md-4">
											<Link to={"/nutrition/recipes/"+similarRecipe.id} className="block recipes__category">
												<div className="recipes__category__picture" style={{background:(similarRecipe.picture ? 'url('+similarRecipe.picture+') no-repeat center center' : ''),backgroundSize:'cover'}}></div>
												<div className="block-title">{similarRecipe.title}</div>
											</Link>
									  </div>
								)
							})
						}
					</div>
				</div>
			}
		</div>
	);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Recipe);

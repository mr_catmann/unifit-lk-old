import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Preloader from '../Preloader';
import Response from '../Response'; 
import * as recipesActions from '../../actions/recipes';
import { bindActionCreators } from 'redux';
const mapStateToProps = (state => {
	return{ 
		...state.recipes
	}
});

const mapDispatchToProps = dispatch => ({
	 actions: bindActionCreators(recipesActions, dispatch)
});

class RecipesCategories extends React.Component {
	constructor() {
		super();
		this.state = {
			
		};
	} 
  
 
  
  componentWillMount() {
		if (!this.props.recipes || !this.props.recipes.categories) {
			 this.props.actions.getRecipesCategories(this.props.id);
		}
  }
  
  render() {
	if (!this.props.recipes || !this.props.recipes.categories  || this.props.recipes.categories.loading) {
		return <div className="recipes"><Preloader /></div>;
	}
	if (this.props.recipes.categories.error) {
		return <Response data={this.props.recipes.categories.error} />
	}
	return <div className="recipes">
		<h3 className="mt-5 mb-5">Книга рецептов</h3>
		<div className="row">
		{
			this.props.recipes.categories.data.map((category, index) => {     
				return (
				      <div key={category.id} className="col-xs-12 col-md-3">
							<Link to={"/nutrition/category/"+category.url} className="block recipes__category">
								<div className="recipes__category__picture" style={{background:(category.random_recipe && category.random_recipe.picture ? 'url('+category.random_recipe.picture+') no-repeat center center' : ''),backgroundSize:'cover'}}></div>
								<div className="block-title">{category.name}</div>
							</Link>
				      </div>
				)
			})
		}
		</div>
	</div>
 }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipesCategories);

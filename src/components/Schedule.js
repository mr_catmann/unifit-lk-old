import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { Link,Redirect } from 'react-router-dom';
import Preloader from './Preloader';
import MenuItem from './MenuItem';
import Measurement from './Measurement';
import MainPagePlots from './MainPagePlots';
import Training from './Training';
const mapStateToProps = state => ({
  ...state.schedule,
});

const mapDispatchToProps = dispatch => ({
  onLoad: (payload) =>
    dispatch({ type: 'SCHEDULE_LOADED', payload }),
  onSetScheduleItem: (payload) =>
    dispatch({ type: 'SCHEDULE_CURRENT_ITEM_CHANGED', payload }),	
});

class Schedule extends React.Component {
  constructor() {
    super();
	this.selectScheduleItem = this.selectScheduleItem.bind(this);
	this.state = {
		linkToRedirect:null,
		currentScheduleItem:null,
		currentScheduleItemIndex:null,
	};
  }
  
  selectScheduleItem(item,index) {
	  if (item.type === "TYPE_MENU") {
		  this.setState({
			  linkToRedirect:'/menu',
		  })
	  } 
	  this.props.onSetScheduleItem({item:item,index:index});
  }
  
  componentWillMount() {
		if (!this.props.schedule || !this.props.schedule.data) {
			this.props.onLoad(agent.Schedule.today());
		}
  }
  
  getButton(item) {
	  return '';
	  switch (item.type) {
			case 'TYPE_MENU':
				return <Link to="/menu" className="btn btn-success btn-contour">В отчет</Link>
			case 'TYPE_MEASUREMENT':
				return '';
			default:
				return '';
	  }
  }

  render() {
	if (!this.props.schedule || !this.props.schedule.data  || !this.props.schedule.data[this.props.schedule.currentDay]) {
		return <div className="col-xs-12 col-md-4"><div className="schedule"><Preloader /></div></div>;
	}
	let currentScheduleItem = this.props.schedule.current.item;
	let schedule = this.props.schedule.data[this.props.schedule.currentDay];
	if (this.props.onlyTypes) {
		schedule = schedule.filter(scheduleItem => this.props.onlyTypes.indexOf(scheduleItem.type) !== -1);
		currentScheduleItem = schedule[this.props.schedule.current.index];
	}
	const currentScheduleItemIndex = this.props.schedule.current.index !== null ? this.props.schedule.current.index  : schedule.indexOf(currentScheduleItem);
	return (
	  <div className="row">
		  { this.state.linkToRedirect && (!this.props.location || this.props.location.pathname !== this.state.linkToRedirect) && <Redirect to = {this.state.linkToRedirect} />}
		  <div className="col-xs-12 col-md-4">
			  <div className="schedule">
			    {this.props.header}
				<div className="schedule__list" >
					{
						schedule.map((key,index) => {
						  return (
							<div data-type={key.type} className={"schedule__item "+(currentScheduleItemIndex === index ? "schedule__item--current" : "")} key={key.name} onClick={(e) => this.selectScheduleItem(key, index, e)}>
								<div className="schedule__item__texts">
								<span className="schedule__item__name">{key.name}</span>
								<span className="schedule__item__time">{key.time}</span>
								</div>
								<div className="schedule__item__buttons">
									{this.getButton(key)}
								</div>
							</div>
						  );
						})
					}
			  </div>
			</div>
		 </div>
		 <div className="col-xs-12 col-md-8">
			 {!currentScheduleItem &&
				<MainPagePlots />
			 }
			 {currentScheduleItem && currentScheduleItem.type == "TYPE_TRAINING" && 
				<Training id={currentScheduleItem.id} />
			 }
			 {currentScheduleItem && currentScheduleItem.type == "TYPE_MENU" && 
				<MenuItem id={currentScheduleItem.id} index={currentScheduleItem.index} />
			 }
			 {currentScheduleItem && currentScheduleItem.type == "TYPE_MEASUREMENT" && 
				<Measurement />
			 }
		 </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);

import React from 'react';
import agent from '../agent';
import Schedule from './Schedule';
import { connect } from 'react-redux';
const mapStateToProps = state => ({
  ...state.schedule,
});

const mapDispatchToProps = dispatch => ({
	onLoad: (payload) =>
		dispatch({ type: 'SCHEDULE_LOADED', payload }),
});
class Menu extends React.Component {
  constructor() {
		super();
		//this.selectScheduleItem = this.selectScheduleItem.bind(this);
		this.state = {
			selectDayMenuShown:false,
			currentSelectedDay:-1,
	     	currentSelectedDayText:'cегодня'
		}
		this.showHideDayMenu = this.showHideDayMenu.bind(this);
		this.selectCurrentDay = this.selectCurrentDay.bind(this);
  }
   
  componentWillMount() {
		
  }

  componentWillUnmount() {
   
  }
  
  selectCurrentDay(item) {
	  this.setState({
		  currentSelectedDay:item.day,
		  currentSelectedDayText:item.name,
		  selectDayMenuShown:false
	  })
	  this.props.onLoad(agent.Schedule.byDay(item.day));
  }
  showHideDayMenu() {
	  this.setState({
		  selectDayMenuShown:!this.state.selectDayMenuShown
	  })
  }

  render() {
	   const header = this.props.schedule && this.props.schedule.availableDays ? <div className="schedule__header">Меню на&nbsp;
			<span className="schedule__day-select">
				<span className="schedule__day-select__current-item" onClick={(e) => this.showHideDayMenu(e)}>{this.state.currentSelectedDayText}</span>
					{this.state.selectDayMenuShown && <span className="schedule__day-select__items">
						{
						  this.props.schedule.availableDays.map((key,index) => {
							  return (
								<a key={key.day} onClick={(e) => this.selectCurrentDay(key, index, e)} className="schedule__day-select__item">{key.name}</a>
							  )
						  })
						}
					</span>}
			</span>
	  </div> : '';
      return (
		<div className="menu-page">
			<div className="container page">
				<Schedule header={header} location={this.props.location} onlyTypes={['TYPE_MENU']} />
			</div>
		</div>
      );
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Menu);

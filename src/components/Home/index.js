import React from 'react';
import Schedule from '../Schedule';
import { connect } from 'react-redux';


const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token
});

const mapDispatchToProps = dispatch => ({

});

class Home extends React.Component {
  componentWillMount() {
    
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
	   const header = <div className="schedule__header">Расписание на сегодня</div>;
	   return (
		   <div className="home-page">
				<div className="container page">
					<Schedule header={header} location={this.props.location} />
				</div>
		   </div>
		);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);

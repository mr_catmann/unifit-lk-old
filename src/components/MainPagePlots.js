import React from 'react'; 
import { connect } from 'react-redux';
import DynamicsPlot from './Plots/DynamicsPlot';
import { bindActionCreators } from 'redux';
import * as plotsActions from '../actions/plots';

const mapDispatchToProps = dispatch => ({
	 actions: bindActionCreators(plotsActions, dispatch)
});
const mapStateToProps = (state => {
	return{ 
		...state.measurement
	}
});

class MainPagePlots extends React.Component {
  constructor() {
    super();
  }

  render() {
	return (
	  <div>
		  <div className="block-title">Твой прогресс</div>
		  <div className="row">
			<div className="col-xs-12 col-md-12">
				<DynamicsPlot getData={this.props.actions.getDynamics}/>
			</div>
		  </div>
      </div>
    );
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(MainPagePlots);

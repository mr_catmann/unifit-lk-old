import React from 'react';
import agent from '../agent';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { Form, Field } from 'react-final-form'
import Preloader from './Preloader';
import Response from './Response';
import Error from './Error';
import SweetAlert from 'sweetalert-react';
import "sweetalert/dist/sweetalert.css";
import { store } from '../store';
import { push } from 'react-router-redux';
import * as measurementActions from '../actions/measurement';
const mapStateToProps = (state => {
	return{ 
		...state.measurement
	}
});

const mapDispatchToProps = dispatch => ({
	 actions: bindActionCreators(measurementActions, dispatch)
});

class Measurement extends React.Component {
  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
	this.validate = this.validate.bind(this);
	this.state = {
		showAlert:true
	};
  }

  componentWillMount() {
	 this.props.actions.getMeasurementData();
  }

  onSubmit(values) {
	this.props.actions.sendMeasurementData(values);
  }
  
  validate(values){
	  const errors = {};
	  if (!this.props.measurement.data) {
			return {};
	  }
      this.props.measurement.data.forEach(item=>{
			if (!values[item.id] || values[item.id]<1) {
				errors[item.id] = "Заполните это поле";
			}
	  })
	  return errors;
  }
  
  render() {
	if (!this.props.measurement || this.props.measurement.loading) {
		return <Preloader />;
	}
	if (this.props.measurement.error) {
		return <Error data={this.props.measurement.error} />;
	}
	let initialValues = {};
	this.props.measurement.data.forEach(item=>{
		let val = item.old_value || item.current_value || 0;
		if (val) {
			initialValues[item.id] = val;
		}
	});
	return (
      <div >
			<SweetAlert
					show={!!(this.state.showAlert && this.props.measurementSendResponse && this.props.measurementSendResponse.data && this.props.measurementSendResponse.data.status)}
					title="Измерения сохранены!"
					onConfirm={() => {
						this.setState({
							showAlert:false
						});
						console.log(this);
						store.dispatch(push('/'));
					}}
				  />
              <h3 className="block-title">Замеры</h3>
              <div className="block">
			  {this.props.measurementSendResponse && this.props.measurementSendResponse.data &&
					<Response data={this.props.measurementSendResponse.data} />	
			  }
			  
			  <Form validate={values => this.validate(values)} /* initialValues={initialValues} */ onSubmit={this.onSubmit}  render={({ handleSubmit, form, submitting, pristine, values }) => (
					<form onSubmit={handleSubmit}> 
						{
							this.props.measurement.data.map(item => {
								return (
									<Field key={item.id} name={item.id} >
										 {({ input, meta }) => (
											<div key={item.id} className="form-group">
													<label htmlFor={item.id}>{item.name}</label>
													<div className="input-group">
														 <input className={`form-control ${meta.touched && meta.error ? 'is-invalid' : ''}`}  {...input} placeholder={initialValues[item.id]} type="number" />
														 <div className="input-group-append">
															<span className="input-group-text">{item.units}</span>
														 </div>
														 <div className="invalid-feedback">
															 {meta.touched ? meta.error : ''}
														 </div>													
													</div>
													<small className="form-text text-muted">{item.description}</small>
											</div>
										)}	
									</Field>
								)
							})
						}
						<button disabled={this.props.measurementSendResponse && this.props.measurementSendResponse.loading} type="submit" className="btn btn-primary">
							Отправить
						</button>
					</form>
				  )}
			  />
            </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Measurement);

import React from 'react';
import { Link } from 'react-router-dom';
class ErrorPage extends React.Component {
  render() {
	  console.log(this.props);
	  return <div className="error-page">
			<h1 className="error-page__code">Ошибка 404</h1>
			<div className="error-page__info-text">Страница не найдена</div>
			<Link className="btn btn-xs btn-primary" to="/">На главную</Link>
	  </div>
  }
}

export default ErrorPage;
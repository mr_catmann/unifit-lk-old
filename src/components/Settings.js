import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { Form, Field } from 'react-final-form'
import Preloader from './Preloader';
import Response from './Response';
import SelectButtons from './SelectButtons';
import { store } from '../store';  
import { push } from 'react-router-redux';
import * as settingsActions from '../actions/settings';
const mapDispatchToProps = dispatch => ({
	 actions: bindActionCreators(settingsActions, dispatch),
	 logout: () =>
		dispatch({ type: "LOGOUT", payload: {} }),
});

const mapStateToProps = (state => {
	return{ 
		...state.settings,
		...state.common,
		...state.settingsSendResponse,
	}
});



class Settings extends React.Component {
	
	constructor() {
		super();
		this.onSubmitSettings = this.onSubmitSettings.bind(this);
		this.validateSettings = this.validateSettings.bind(this);
		this.logout = this.logout.bind(this);
	}

	logout() {
		this.props.logout();
	}
	
  _textInput(item,input,meta) {
	  return (
		  <div key={item.id}>
			<label htmlFor={item.id}>{item.name}</label>
			<div className="input-group">
				<input className={`form-control ${meta.touched && meta.error ? 'is-invalid' : ''}`}  {...input} type="number" />
				<div className="input-group-append">
					{item.units && <span className="input-group-text">{item.units}</span>}
				</div>
				<div className="invalid-feedback">
						{meta.touched ? meta.error : ''}
				</div>													
			</div>
			<small className="form-text text-muted">{item.description}</small>
		 </div>
	  )
  }	
  
  
  
  onChangeSelection(e) {
		
  }
  
  onSubmitSettings(values) {
		this.props.actions.saveSettings(values);
  }
  
  validateSettings(values) {
	  let errors = [];
	  let items = [
		  {id:'target_weight'},
	  ];
	  items.forEach(item=>{
			if (!values[item.id] || values[item.id]<1) {
				errors[item.id] = "Заполните это поле";
			}
	  })
	  return errors;
  }
  
  render() {
	const trainingsTypes = [
		{id:1,name:'Стандартная'},
		{id:2,name:'Повышенная'},
	]
	const nutritionTypes = [
		{id:1,name:'Стандартное'},
		{id:2,name:'Вегетарианское'},
	];
	const currentUser = this.props.currentUser;
	const initialValues = {
		target_weight:currentUser.target_weight,
		trainings_type:currentUser.trainings_type,
		nutrition_type:currentUser.nutrition_type
	}
	return (
      <div className="row">
		<div className="col-xs-12 col-md-12">
			<h3 className="mt-5 mb-3">Настройки</h3>
			{this.props.settingsSendResponse &&
					<Response data={this.props.settingsSendResponse} />	
			 }
			<Form validate={values => this.validateSettings(values)} initialValues={initialValues} onSubmit={this.onSubmitSettings}  render={({ handleSubmit, form, submitting, pristine, values }) => (
					<form onSubmit={handleSubmit}> 
						<Field
						name="target_weight"
						render={({ input, meta }) => this._textInput({id:'target_weight',name:'Цель по весу',units:'кг'},input,meta)}
						/>
						<div className="row">
							<div className="col-xs-12 col-md-6">
								<Field
								name="trainings_type"
								title="Сложность тренировок"
								variants={trainingsTypes}
								component={SelectButtons}
								/>
							</div>
							<div className="col-xs-12 col-md-6">
								<Field
								name="nutrition_type"
								title="Тип питания"
								variants={nutritionTypes}
								component={SelectButtons}
								/>
							</div>
						</div>
						<hr/>
						<button type="submit" disabled={this.props.settingsSendResponse && this.props.settingsSendResponse.loading} className="btn btn-primary">
							Сохранить
						</button>
					
						
					</form>
				 )}
			  />
			  <hr /> 
			  <button onClick={(e) => this.logout(e)} className="btn btn-outline-danger" >Выйти</button>
		 </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);

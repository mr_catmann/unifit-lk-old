import React from 'react';
import { connect } from 'react-redux';
import Response from '../Response';
import Preloader from '../Preloader';
import SelectButtons from '../SelectButtons';
import {XYPlot, XAxis, YAxis, HorizontalGridLines, LineSeries} from 'react-vis';
const mapStateToProps = (state => {
	return{ 
		...state.plots
	}
});

class DynamicsPlot extends React.Component {
  constructor() {
    super();
	this.state = {
		currentPlotName:'weight',
	}
	this.setCurrentPlotName = this.setCurrentPlotName.bind(this);
  }
  componentWillMount() {
	this.props.getData();
  }
  setCurrentPlotName(name) {
	  this.setState({
			currentPlotName:name  
	  });
  }
  render() {
	const dataLoaded = this.props.plots && this.props.plots.dynamics && this.props.plots.dynamics.data && this.props.plots.dynamics.data.status === 1;
	const currentData = dataLoaded ? this.props.plots.dynamics.data.data : null;
	const plotData = (currentData && currentData[this.state.currentPlotName]) ? currentData[this.state.currentPlotName].values.map(val=>{
		return {
			x:val.week,
			y:val.value
		}
	}) : null;
	const graphNames = currentData ? Object.keys(currentData).map(key=>{
		return {
			id:key,
			name:currentData[key].name
		}
	}) : null;
	return (
	  <div>
	    {this.props.plots && this.props.plots.dynamics && this.props.plots.dynamics.loading && <Preloader/>}
		{this.props.plots && this.props.plots.dynamics && this.props.plots.dynamics.data && this.props.plots.dynamics.data.status === 0 && <Response data={this.props.plots.dynamics.data} /> }
		{dataLoaded && 
		<div className="block">
			<div className="block-header">
				<div className="block-header__title">Динамика по замерам</div>
			</div>
			{plotData ?
				<div>
				<SelectButtons onChange={(e) => this.setCurrentPlotName(e)} customClasses="select-buttons--small select-buttons--grey" variants={graphNames} />
				<XYPlot 
				  width={300}
				  height={150}>
				  <HorizontalGridLines />
				 
				  <XAxis />
				  <YAxis />
				   <LineSeries
				  color="#c94ff3"
					data={plotData}/>
				</XYPlot>
				</div> 
				:
				<div className="block__no-data">
					<div className="block__no-data__header">Недостаточно данных</div>
					<div className="block__no-data__text">График появится тут после следующего измерения</div>
				</div>
			}
		</div>
		}
	  </div>
    );
  }
}

export default connect(mapStateToProps)(DynamicsPlot);

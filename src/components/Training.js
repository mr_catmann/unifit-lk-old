import React from 'react';
import { connect } from 'react-redux';
import Preloader from './Preloader';
import Error from './Error';
import * as trainingActions from '../actions/trainings';
import { bindActionCreators } from 'redux'
const mapStateToProps = (state => {
	return{ 
		...state.trainings
	}
});

const mapDispatchToProps = dispatch => ({
	 actions: bindActionCreators(trainingActions, dispatch)
});


class Training extends React.Component {
  constructor() {
		super();
		this.state = {
			
		};
  }
  
  training() {
		if (!this.props.trainings || !this.props.trainings.items || !this.props.trainings.items[this.props.id]) {
			return null;
		}
		return this.props.trainings.items[this.props.id];
  }
  
  componentWillMount() {
		if (!this.training()) {
			 this.props.actions.getTraining(this.props.id);
		}
  }
  
  render() {
	const training = this.training();
	const index = this.props.index;
	console.log(training);
	if (!training) {
		return <div className="training"><Preloader /></div>;
	}
	return <div className="training">
		<div className="block-title">{training.title}</div>
		{
			training.data.exercises.map((exercise, index) => {     
				const exerciseText = exercise && exercise.text ? {'__html':exercise.text} : null;
				return (
				      <div className="block block-item" key={index}>
							{exercise.video_url && 
							<div className="block-item__left">	
								<iframe className="block-item__left__video" src={exercise.video_iframe} frameBorder="0" allowFullScreen></iframe>
							</div>
							}
							<div className="block-item__info">
								<div className="block-item__name">{exercise.title}</div>
								{
									(exercise.text && 
									<div className="block-item__text">
										<div className="block-item__text__contents" dangerouslySetInnerHTML={exerciseText}></div>
									</div>
									)
								}
							</div>
				      </div>
				)
			})
		}
	</div>
 }
}

export default connect(mapStateToProps, mapDispatchToProps)(Training);

import React from 'react';

class SelectButtons extends React.Component {
	
	constructor(props) {
		super(props);
		const input = props.input;
		const hasValue = (input && input.value && input.value !== "");
		this.state = {
			currentSelectedItem:((hasValue) ? input.value : props.variants[0].id),
		};
		this.setVariant = this.setVariant.bind(this);
		
	}
	
	setVariant(variant) {
		this.setState({
			currentSelectedItem:variant.id
		})
		this.props.onChange && this.props.onChange(variant.id);
		this.props.input && this.props.input.onChange && this.props.input.onChange(variant.id);
	} 
	componentDidUpdate() {
		
	}
	
	componentDidMount() {
		this.setVariant(this.props.variants[0]);
	}
	
	render() {
		const meta = this.props.meta;
		if (!this.props.variants) return;
		
		return (
			<div className="form-group">
				<label htmlFor={this.props.name}>{this.props.title}</label>
				<div className={"select-buttons "+(this.props.customClasses ? this.props.customClasses : "")}>
					{
						this.props.variants.map((key,index) => {
							return (
								<a onClick={(e) => this.setVariant(key, e)} key={key.id}  className={"select-buttons__item "+(this.state.currentSelectedItem == key.id ? "select-buttons__item--current" : "")}>
									<span className="select-buttons__item__element"></span>
									<span className="select-buttons__item__text">{key.name}</span>
								</a>
							)
						})
					}
				</div>
				<div className="invalid-feedback">
						{meta && meta.touched ? meta.error : ''}
				</div>	
				{this.props.description && <small className="form-text text-muted">{this.props.description}</small> }				
			</div>
		);
	}
}

export default SelectButtons;

import React from 'react';

class Preloader extends React.Component {
  render() {
	return (
      <div className="preloader__container">
		<div className="preloader__inner">
			<div className="preloader"></div>
		</div>
      </div>
    );
  }
}

export default Preloader;

import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/unifit_logo.png'

class Header extends React.Component {
	
 constructor() {
    super();
    this.state = {
		navbarExpanded:false,
		links:[
			{name:'Главная',to:'/'},
			{name:'Питание',to:'/nutrition'},
		//	{name:'Тренировки',to:'/trainings'},
		//	{name:'Вебинары',to:'/webinars'},
			{name:'Настройки',to:'/settings'},
		]
    };
	this.toggleNavbar = this.toggleNavbar.bind(this);
  }
  
  toggleNavbar() {
	  this.setState({
		  navbarExpanded:!this.state.navbarExpanded
	  });
  }
  
  render() {
	return (
      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container">
          <Link to="/" className="navbar-brand">
			<img src={logo} alt="Unifit" className="navbar-logo" />
			<span className="navbar-slogan">Акселератор красоты и здоровья</span>
          </Link>
		  <button onClick={(e) => this.toggleNavbar(e)} className="navbar-toggler" type="button">
			<span className="navbar-toggler-icon"></span>
		  </button>
		  <div className={"collapse navbar-collapse "+((this.state.navbarExpanded) ? "show" : "")}>
			  <div className="navbar-nav">	
					{
						this.state.links.map(key => {
							return (
								<li className="nav-item" key={key.to}>
								  <Link className="nav-link" to={key.to}>{key.name}</Link>
								</li>
							)
						})
					}
				</div>
		  </div>
        </div>
      </nav>
    );
  }
}

export default Header;

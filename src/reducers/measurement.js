export default (state = {}, action) => {
  switch (action.type) {
    case 'MEASUREMENT_DATA_SUCCESS':
      return {
        ...state,
        measurement: {
			data: action.payload,
			loading:false,
			error:null,
		}
      };
	case 'MEASUREMENT_DATA_REQUEST':
      return {
        ...state,
        measurement: {
			loading:true,
			error:null,
		}
      };
    case 'MEASUREMENT_DATA_ERROR':
      return {
        ...state,
        measurement: {
			loading:false,
			error:action.payload,
		}
    };
	 case 'MEASUREMENT_SEND_REQUEST':
      return {
        ...state,
        measurementSendResponse: {
			loading:true,
		}
      };
	case 'MEASUREMENT_SEND_RESPONSE':
      return {
        ...state,
        measurementSendResponse: {
			loading:false,
			data:action.payload
		}
      };

    default:
      return state;
  }
};

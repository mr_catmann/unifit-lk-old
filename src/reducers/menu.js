export default (state = {}, action) => {
  let items = state.menu ? (state.menu.items || []) : [];
  switch (action.type) {
    case 'MENU_REQUEST':
	  
      return {
        ...state,
        menu: {
			loading:true,
			error:null,
			items,
		}
      };
	case 'MENU_SUCCESS':
	  items[action.payload.id] = action.payload;
	  return {
        ...state,
        menu: {
			loading:false,
			error:null,
			items
		}
      };
    
    default:
      return state;
  }
};

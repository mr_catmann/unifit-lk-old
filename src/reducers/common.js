
const defaultState = {
  appName: 'Unifit',
  token: null,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'APP_LOAD':
      return {
        ...state,
        token: action.token || null,
        appLoaded: true,
        currentUser: action.payload ? action.payload.user : null
      };
    case 'REDIRECT':
      return { ...state, redirectTo: null };
    case 'LOGOUT':
      return { ...state, redirectTo: '/', token: null, currentUser: null };
    case 'LOGIN':
    case 'REGISTER':
      return {
        ...state,
        redirectTo: action.payload.status === 0 ? null : '/',
        token: action.payload.status === 0 ? null : action.payload.token,
        currentUser: action.payload.status === 0 ? null : action.payload.user
      };
    default:
      return state;
  }
};

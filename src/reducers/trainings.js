export default (state = {}, action) => {
  let items = state.trainings ? (state.trainings.items || []) : [];
  switch (action.type) {
    case 'TRAININGS_REQUEST':
      return {
        ...state,
        trainings: {
			loading:true,
			error:null,
			items,
		}
      };
	case 'TRAININGS_SUCCESS':
	  items[action.payload.id] = action.payload;
	  return {
        ...state,
        trainings: {
			loading:false,
			error:null,
			items
		}
      };
    
    default:
      return state;
  }
};

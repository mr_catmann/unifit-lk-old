export default (state = {}, action) => {
  switch (action.type) {
    case 'SCHEDULE_LOADED':
	  let scheduleData = state.schedule && state.schedule.data ? state.schedule.data : {};
	  scheduleData[action.payload.day] = action.payload.list;
	  let currentItemData = state.schedule && state.schedule.current ? state.schedule.current : {item:null,index:null};
	  if (currentItemData.index !== null) {
		  if (currentItemData.index <= action.payload.list.length) {
			  currentItemData.item = action.payload.list[currentItemData.index];
		  } else {
			  currentItemData.item = action.payload.list[0];
		  }
	  }
	  return {
        ...state,
        schedule: {
			...state.schedule,
			data:scheduleData,
			availableDays:action.payload.available_days,
			currentDay:action.payload.day,
			current:currentItemData,
		}
      };
	case 'SCHEDULE_CURRENT_ITEM_CHANGED':
      return {
        ...state,
        schedule: {
			...state.schedule,
			current:{
				item:action.payload.item,
				index:action.payload.index
			}
		}
      };  
    default:
      return state;
  }
};

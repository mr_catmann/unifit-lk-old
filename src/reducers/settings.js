

export default (state = {}, action) => {
  switch (action.type) {
    case "SETTINGS_SAVE_REQUEST":
      return {
        ...state,
        settingsSendResponse:{
			loading:true,
		}
      };
    case "SETTINGS_SAVE_RESPONSE":
	  const payload = action.payload;
	  if (payload.status) {
		return {
			...state,
			settingsSendResponse:payload,
			currentUser:payload.data
		}
	  } else {
		  return {
			...state,
			settingsSendResponse:payload
		  }
	  }
    default:
      return state;
  }
};

export default (state = {}, action) => {
  switch (action.type) {
    case "PLOTS_DYNAMICS_REQUEST":
      return {
		 ...state,
		 plots: {
			...state.plots,
			dynamics:{
				loading:true,
				data:null
			}
		 }	
	  }
	case "PLOTS_DYNAMICS_RESPONSE":
      return {
		 ...state,
		 plots: {
			...state.plots,
			dynamics:{
				loading:false,
				data:action.payload,
			}
		 }	
	  }
    default:
      return state;
  }
};

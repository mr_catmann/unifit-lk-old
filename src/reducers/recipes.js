export default (state = {}, action) => {
  switch (action.type) {
    case 'RECIPES_CATEGORIES_REQUEST':
      return {
        ...state,
        recipes:{
			...state.recipes,
			categories:{
				loading:true,
				error:null,
			}
		}
      };
	case 'RECIPES_CATEGORIES_SUCCESS':
	  return {
        ...state,
        recipes:{
			...state.recipes,
			categories:{
				loading:false,
				error:null,
				data:action.payload,
			}
		}
      };
    case 'RECIPES_CATEGORIES_ERROR':
		return {
        ...state,
        recipes:{
			...state.recipes,
			categories:{
				loading:false,
				error:action.payload
			}
		}
      };
	case 'RECIPES_CATEGORY_REQUEST':
      return {
        ...state,
        recipes:{
			...state.recipes,
			listsByCategory:{
				loading:true,
				error:null,
			}
		}
      };
	case 'RECIPES_CATEGORY_SUCCESS':
	  let list = state.recipes && state.recipes.listsByCategory && state.recipes.listsByCategory.data ? state.recipes.listsByCategory.data : {};
	  list[action.payload.category_url] = action.payload;
	  return {
        ...state,
        recipes:{
			...state.recipes,
			listsByCategory:{
				loading:false,
				error:null,
				data:list
			}
		}
      };
    case 'RECIPES_CATEGORY_ERROR':
		return {
        ...state,
        recipes:{
			...state.recipes,
			listsByCategory:{
				loading:false,
				error:action.payload
			}
		}
      };
	case 'RECIPES_ITEM_REQUEST':
      return {
        ...state,
        recipes:{
			...state.recipes,
			list:{
				loading:true,
				error:null,
			}
		}
      };
	case 'RECIPES_ITEM_SUCCESS':
	  let listById = state.recipes && state.recipes.list && state.recipes.list.data ? state.recipes.list.data : {};
	  listById[action.payload.data.id] = action.payload;
	  return {
        ...state,
        recipes:{
			...state.recipes,
			list:{
				loading:false,
				error:null,
				data:listById
			}
		}
      };
    case 'RECIPES_ITEM_ERROR':
		return {
        ...state,
        recipes:{
			...state.recipes,
			list:{
				loading:false,
				error:action.payload
			}
		}
      };    
    default:
      return state;
  }
};

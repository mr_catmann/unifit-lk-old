export default (state = {}, action) => {
  switch (action.type) {
    case 'CHECK_REGISTRATION_CODE_REQUEST':
	  return {
        ...state,
        register:{
			...state.register,
			code:{
				loading:true,
				error:null,
				user:null,
			}
		}
      };
	case 'CHECK_REGISTRATION_CODE_RESPONSE':
	  return {
        ...state,
        register: {
			...state.register,
			code:{
				loading:false,
				error:action.payload.status ? null : action.payload,
				user:action.payload.status ? action.payload.user : null
			}
		}
      };  
	case 'SAVE_PASSWORD_REQUEST':
	  return {
        ...state,
        register:{
			...state.register,
			password:{
				loading:true,
				response:null,
			}
		}
      };
	case 'SAVE_PASSWORD_RESPONSE':
	  return {
        ...state,
        register: {
			...state.register,
			password:{
				loading:false,
				response:action.payload,
			}
		}
      };   
	case 'SAVE_FORM_DATA_REQUEST':
	  return {
        ...state,
        register:{
			...state.register,
			save_form:{
				loading:true,
				response:null,
			}
		}
      };
	case 'SAVE_FORM_DATA_RESPONSE':
	  return {
        ...state,
        register: {
			...state.register,
			save_form:{
				loading:false,
				response:action.payload,
			}
		}
      };   
    default:
      return state;
  }
};

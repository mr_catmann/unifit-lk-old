import agent from '../agent';
export const getMenu = (id) => {
    return (dispatch) => {
		dispatch({
		  type: 'MENU_REQUEST',
		  payload: {},
		})

		agent.Menu.get(id).then(res=>{
		  res.status ? 
			  dispatch({
				type: 'MENU_SUCCESS',
				payload: res.data,
			  }) 
		   : 
			dispatch({
				type: 'MENU_ERROR',
				payload: res,
			}) 
		})
	}
}


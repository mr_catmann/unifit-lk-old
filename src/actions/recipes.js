import agent from '../agent';
export const getRecipesCategories = () => {
    return (dispatch) => {
		dispatch({
		  type: 'RECIPES_CATEGORIES_REQUEST',
		  payload: {},
		})
		agent.Recipes.getCategories().then(res=>{
			res.status ? 
			  dispatch({
				type: 'RECIPES_CATEGORIES_SUCCESS',
				payload: res.data,
			  }) 
		   : 
			dispatch({
				type: 'RECIPES_CATEGORIES_ERROR',
				payload: res,
			}) 
		})
	}
}

export const getRecipesByCategory = (id) => {
    return (dispatch) => {
		dispatch({
		  type: 'RECIPES_CATEGORY_REQUEST',
		  payload: {},
		})
		agent.Recipes.getByCategory(id).then(res=>{
			res.status ? 
			  dispatch({
				type: 'RECIPES_CATEGORY_SUCCESS',
				payload: res,
			  }) 
		   : 
			dispatch({
				type: 'RECIPES_CATEGORY_ERROR',
				payload: res,
			}) 
		})
	}
}

export const getRecipeById = (id) => {
    return (dispatch) => {
		dispatch({
		  type: 'RECIPES_ITEM_REQUEST',
		  payload: {},
		})
		agent.Recipes.getById(id).then(res=>{
			res.status ? 
			  dispatch({
				type: 'RECIPES_ITEM_SUCCESS',
				payload: res,
			  }) 
		   : 
			dispatch({
				type: 'RECIPES_ITEM_ERROR',
				payload: res,
			}) 
		})
	}
}

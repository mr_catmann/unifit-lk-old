import agent from '../agent';
export const getMeasurementData = () => {
    return (dispatch) => {
		dispatch({
		  type: 'MEASUREMENT_DATA_REQUEST',
		  payload: {},
		})

		agent.Measurement.data().then(res=>{
		  res.status ? 
			  dispatch({
				type: 'MEASUREMENT_DATA_SUCCESS',
				payload: res.data,
			  }) 
		   : 
			dispatch({
				type: 'MEASUREMENT_DATA_ERROR',
				payload: res,
			  }) 
		})
	}
}
export const sendMeasurementData = (data) => {
	return (dispatch) => {
		dispatch({
		  type: 'MEASUREMENT_SEND_REQUEST',
		  payload: {},
		})
		agent.Measurement.send(data).then(res=>{
			dispatch({
				type: 'MEASUREMENT_SEND_RESPONSE',
				payload: res,
			}) 
		})
	}
}


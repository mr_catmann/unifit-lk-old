import agent from '../agent';
export const checkRegistrationCode = (code) => {
    return (dispatch) => {
		dispatch({
			type: 'CHECK_REGISTRATION_CODE_REQUEST',
			payload: {},
		})

		agent.Register.checkRegistrationCode(code).then(res=>{
			  dispatch({
					type: 'CHECK_REGISTRATION_CODE_RESPONSE',
					payload: res,
			  }) 
		})
	}
}

export const savePassword = (data) => {
    return (dispatch) => {
		dispatch({
			type: 'SAVE_PASSWORD_REQUEST',
			payload: {},
		})

		agent.Register.changePassword(data).then(res=>{
			  dispatch({
					type: 'SAVE_PASSWORD_RESPONSE',
					payload: res,
			  }) 
		})
	}
}

export const saveFormData = (data) => {
    return (dispatch) => {
		dispatch({
			type: 'SAVE_FORM_DATA_REQUEST',
			payload: {},
		})

		agent.Register.saveFormData(data).then(res=>{
			  dispatch({
					type: 'SAVE_FORM_DATA_RESPONSE',
					payload: res,
			  }) 
		})
	}
}
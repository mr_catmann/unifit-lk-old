import agent from '../agent';
export const getTraining = (id) => {
    return (dispatch) => {
		dispatch({
		  type: 'TRAININGS_REQUEST',
		  payload: {},
		})
		agent.Trainings.get(id).then(res=>{
		  res.status ? 
			  dispatch({
				type: 'TRAININGS_SUCCESS',
				payload: res.data,
			  }) 
		   : 
			dispatch({
				type: 'TRAININGS_ERROR',
				payload: res,
			}) 
		})
	}
}


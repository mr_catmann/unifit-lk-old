import agent from '../agent';
export const getDynamics = () => {
    return (dispatch) => {
		dispatch({
			type: 'PLOTS_DYNAMICS_REQUEST',
			payload: {},
		})

		agent.Plots.getDynamics().then(res=>{
			  dispatch({
					type: 'PLOTS_DYNAMICS_RESPONSE',
					payload: res,
			  }) 
		})
	}
}

import agent from '../agent';
export const saveSettings = (data) => {
    return (dispatch) => {
		dispatch({
		  type: 'SETTINGS_SAVE_REQUEST',
		  payload: {},
		})
		agent.Settings.save(data).then(res=>{	  
			dispatch({
				type: 'SETTINGS_SAVE_RESPONSE',
				payload: res,
			}) 
		})
	}
}


import auth from './reducers/auth';
import { combineReducers } from 'redux';
import common from './reducers/common';
import settings from './reducers/settings';
import menu from './reducers/menu';
import measurement from './reducers/measurement';
import schedule from './reducers/schedule';
import plots from './reducers/plots';
import trainings from './reducers/trainings';
import recipes from './reducers/recipes';
import register from './reducers/register';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
  auth,
  common,
  measurement,
  menu,
  settings,
  plots,
  recipes,
  register,
  trainings,
  schedule,
  router: routerReducer
});
